import os
import random

words = ['CHAIR', 'BOOK', 'DESK', 'WALL', 'BALL', 'LIGHT', 'MOUSE', 'MOOSE',
         'BRICK', 'CAT', 'DOG', 'DOC']
word = ''


def get_word():
    return random.choice(words)


def main_menu():
    return '------------------'\
           '\nMain menu choices'\
           '\nNew game:  1'\
           '\nHelp:      2'\
           '\nQuit game: 0'\
           '\n------------------'


def game_menu():
    return '------------------'\
           '\nGame choices'\
           '\nNew word:  NEW'\
           '\nHint:      HINT'\
           '\nHelp:      HELP'\
           '\nReturn to main menu: EXIT'\
           '\n------------------'


def help():
    return '------------------'\
           '\nIn the main menu:'\
           '\nEnter only numbers'\
           '\nIn the game menu:'\
           '\nEnter single letters or full words'\
           '\nYou can write in lower or upper case'\
           '\n------------------'


def parse_choice(choice):
    if (choice == '0'):
        print('You chose to exit.')
        return False
    if (choice == '1'):
        print('You chose a new game.')
        global word
        word = get_word()
        return True
    if (choice == '2'):
        print('You chose help.')
        print help()
    return False


def get_hint(letters):
    global word
    temp = list(word)
    hint = False

    while (not hint):
        letter = random.choice(temp)
        if (letter not in letters):
            hint = True
    return 'Type the letter: ' + letter


def fill_underscore(underscore='', letters=[]):
    global word

    if (underscore == ''):
        for x in range(len(word)):
            underscore += '_'
    underscore = list(underscore)
    for l in letters:
        if (l in word):
            for i, c in enumerate(word):
                if (l == c):
                    underscore[i] = c
    return ''.join(underscore)


def main_game():
    global word
    game_loop = True
    is_complete = False
    guess_list = []
    message = ''
    underscore = fill_underscore()

    while (game_loop):
        os.system('clear')
        my_string = ', '.join(guess_list)

        print game_menu()
        if message:
            print message
            message = ''
        if (not my_string):
            my_string = 'Nothing'
        print('The word length is {0}'
              '\nSo far you guessed: {1}'
              '\nThe word: {2}'
              .format(len(word), my_string, underscore))

        choice = raw_input('Enter a letter: ').upper()

        if (not is_complete):
            if (len(choice) == 1):
                if (choice not in guess_list):
                    guess_list.append(choice)
                    underscore = fill_underscore(underscore, guess_list)
                    if (underscore == word):
                        message = 'You completed the word!'
                        underscore = word
                        is_complete = True
                else:
                    message = 'You already chose this letter. '\
                              'Choose another one.'
            elif (choice == word):
                is_complete = True
                guess_list.append(choice)
                underscore = word
                message = 'You guessed correctly!'
        else:
            message = 'You completed the word!'
        if (choice == 'EXIT'):
            game_loop = False
        elif (choice == 'HELP'):
            message = help()
        elif (choice == 'NEW'):
            word = get_word()
            is_complete = False
            underscore = fill_underscore()
            del guess_list[:]
            message = 'New word!'
        elif (choice == 'HINT'):
            if (not is_complete):
                message = get_hint(guess_list)
            else:
                message = 'The word is complete!'
        elif (not is_complete and len(choice) > 1):
            if (choice not in guess_list):
                guess_list.append(choice)
            message = 'You entered an invalid word!'
    print('You chose to quit the current game.')


choice = ''
os.system('clear')
while (choice != '0'):
    print main_menu()
    choice = raw_input('Enter your choice: ')
    if (parse_choice(choice)):
        main_game()
    elif (choice == '1'):
        print help()
